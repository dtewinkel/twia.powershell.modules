﻿param(
	[Parameter()] $TargetDir
)

cd $TargetDir

# Comment the following line to disable running the tests after each build.
.\Invoke-Tests.ps1
