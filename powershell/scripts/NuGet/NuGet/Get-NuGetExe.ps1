function Get-NuGetExe {
	<#
	.SYNOPSIS
		Get the NuGet executable from the Internet.
		
	.DESCRIPTION
		Get the NuGet executable (NuGet.exe) from NuGet.Org.

	.PARAMETER Path
		Where to download the executable to. Defaults to the current directory.

	.PARAMETER Force
		Download the executable, even if it already exists.

	.PARAMETER WhatIf
		Shows what would happen if Install-Module runs. No directories are created and no download is performed.
		
	.EXAMPLE
		Install-Module Environment -ToUser 

		Install the module NuGet into the user module directory. It will try all registered sources.

	#>

    [CmdletBinding(
		SupportsShouldProcess = $true
		)]
	param (
		[Parameter(
				)]
			[Switch] $Force = $false,

		[Parameter(
				)]
			[string] $Path = "."
    )

	process {
		$exeFile = "$Path\NuGet.exe"
		$url = "https://nuget.org/nuget.exe"
		if ( -not ( Test-Path $Path ) ) {
			mkdir $Path
		}
		if ( -not ( Test-Path $exeFile ) -or $Force ) {
			if($PSCmdlet.ShouldProcess($url, "Downloading '$exeFile'")) {
				Write-Verbose "Downloading NuGet.exe from: $url."
				Invoke-WebRequest $url -OutFile $exeFile 
			}
		}
		if(Test-Path $exeFile) {
			$fileInfo = Get-Item $exeFile 
			Write-Verbose "NuGet.exe version: $($fileInfo.VersionInfo.ProductVersion)."
			$fileInfo
		}
	}
}

if($loadingModule) {
	Export-ModuleMember -Function 'Get-NuGetExe'
}
