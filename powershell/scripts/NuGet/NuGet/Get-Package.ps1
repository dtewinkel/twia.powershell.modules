function Get-Package {
	<#
	.SYNOPSIS
		Install a module from a NuGet source, or from a local file.
		
	.DESCRIPTION
		Installs a new module into the selected target. A target can be one of the registered module directories,
		or a new directory. Modules can be downloaded from NuGet or a NuGet compatible source.
		A module will not be overwritten if it already exists.
		The path will be created if needed.

	.PARAMETER ToUser
		Install the module into the user module path ($home\Documents\WindowsPowerShell\Modules).

	.PARAMETER ToGlobal
		Install the module into the global modules path ($pshome\Modules).

	.PARAMETER Path
		Install the module into the given path. 

	.PARAMETER AddToPsModulePath
		Add the path into the PsModulePath environment variable.

	.PARAMETER WhatIf
		Shows what would happen if Install-Module runs. No module is installed.
		
	.EXAMPLE
		Install-Module Environment -ToUser 

		Install the module NuGet into the user module directory. It will try all registered sources.

	#>

    [CmdletBinding(
		SupportsShouldProcess = $true
		)]
    param(
        [Parameter(
			Mandatory = $true,
            Position=0, 
			ValueFromPipeline = $true,
			ValueFromPipelinebyPropertyName = $true,
			ValueFromRemainingArguments = $true,
			HelpMessage = "Enter the name of the module to install."
			)]
		[ValidateNotNullOrEmpty()]
        [string[]] $Name,
		
		[Parameter(
			)]
		[ValidateNotNullOrEmpty()]
		[string] $Path = ".",

		[Parameter()]
		[switch] $NoVersion = $true
    )

	begin {
		# make sure we have a NuGet.exe
		Get-NuGetExe -Path $NuGetExePath 
				 
		if(-not (Test-Path $Path)) {
			md $Path  | Out-Null
            Write-Verbose -Message "Created path '$Path'."
		}

	}

	process {
		foreach ( $packageName in $Name ) {
			$modulePath = Join-Path $Path $packageName
			if(Test-Path  $modulePath) {
				throw (new-Object System.InvalidOperationException -ArgumentList "Module '$packageName' already exists.")
			}

			md $modulePath | Out-Null
            Write-Verbose -Message "Created path '$modulePath'."
		}
	}
}

New-Alias -Name 'gpa' -Value 'Get-Package' -Force

if($loadingModule) {
	Export-ModuleMember -Function 'Get-Package'
	Export-ModuleMember -Alias 'gpa'
}
