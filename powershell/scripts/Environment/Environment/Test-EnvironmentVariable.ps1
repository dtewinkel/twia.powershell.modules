﻿function Test-EnvironmentVariable {
	<#
	.SYNOPSIS
		Test if an environment exists.
		
	.DESCRIPTION
		Test if an environment exists.
		
	.PARAMETER Name
		The name of the environment variable to test. 

	.PARAMETER Target
		The target to test the environment variable is in.

	.EXAMPLE
		Test-EnvironmentVariable Path

		Test if the Path environment variable exists.

	.EXAMPLE
		Test-EnvironmentVariable Path -User
		
		Test if the Path environment variable exists in the user environment.
	#>
	
    [CmdletBinding()]
    param(
        [Parameter(
			Mandatory = $true,
            Position = 0, 
			ValueFromPipeline = $true,
			ValueFromPipelinebyPropertyName = $true,
			ValueFromRemainingArguments = $true
			)]
        [string] $Name,

		[Parameter()]
		[ValidateSet("User", "Machine", "Process")]
		[string] $Target = "Process"
    )
	
	process {	
		[Environment]::GetEnvironmentVariable($Name, $Target) -ne $null
	}
}

if($loadingModule) {
	Export-ModuleMember -Function 'Test-EnvironmentVariable'
}

