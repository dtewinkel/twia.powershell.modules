function Get-EnvironmentVariable {
	<#
	.SYNOPSIS
		Get environment one or more variables.
		
	.DESCRIPTION
		Get one ore more environment variables from the Windows process environment. 
		All environment variable are returned if no name or names are provided.
		Names can be provided through the -Name parameter or from the pipeline.
		
	.PARAMETER Name
		The name or names of the environment variables to retrieve. This parameter is optional.

	.PARAMETER Strict
		Generate an error instead of a warning if a name is provided for an environment variable that does not exists.

	.PARAMETER Target
		The target to get the environment variable from.

	.EXAMPLE
		Get-EnvironmentVariable Path

		Get the Path environment variable.

	.EXAMPLE
		Get-EnvironmentVariable Path tmp
		
		Get the Path and the tmp environment variables.
		
	.EXAMPLE
		"Tmp" | Get-EnvironmentVariable
		
		Get the tmp environment variable.

	.EXAMPLE
		Get-EnvironmentVariable -Name tmp, path
		
		Get the Path and the tmp environment variables.

	.EXAMPLE
		Get-EnvironmentVariable
		
		Get all environment variables.
	#>
	
    [CmdletBinding()]
    param(
        [Parameter(
            Position=0, 
			ValueFromPipeline = $true,
			ValueFromPipelinebyPropertyName = $true,
			ValueFromRemainingArguments = $true
			)]
        [string[]] $Name,
		
		[Parameter()]
		[switch] $Strict = $false,
		
		[Parameter()]
		[string] $Filter,

		[Parameter()]
		[ValidateSet("User", "Machine", "Process")]
		[string] $Target = "Process"
    )
	

	process {
		if ( $Name ) {
			$Name | foreach {
                $found = $false
				$varName = $_;

				$varPath = "env:$_";
				Write-Verbose -Message "Retrieving environment variable '$varName'."
				try {
					dir $varPath -ErrorAction Stop | % {
						$nameToTest = $_.Name
						$val = [Environment]::GetEnvironmentVariable($nameToTest, $Target)
						if($val -ne $null) {
                            $found = $true
							@{ $nameToTest = $val }
						}
					}
				}
				catch {
					Not-Found $varName
				}
			    if ( -not $found ) {
				    Not-Found $varName
			    }
			}
		}
		else {
			Write-Verbose -Message "Retrieving all environment variables."
			[Environment]::GetEnvironmentVariables($Target)
		}
	}
}

function Not-Found( $varName ) {
	if ($Strict) {
		Write-Error -Message "Environment variable '$varName' not found" `
					-RecommendedAction "Create the environment variable '$varName' or remove it from the parameters." `
					-Activity "Get-EnvironmentVariable" -Reason "EnvironmentVariableNotFound" `
					-Category ObjectNotFound `
					-TargetObject $varName -ErrorId "EnvVarNotFound" 
	}
	else {
		Write-Warning -Message "Environment variable '$varName' not found."
	}
}

Set-Alias env Get-EnvironmentVariable 

if($loadingModule) {
	Export-ModuleMember -Function 'Get-EnvironmentVariable'
	Export-ModuleMember -Alias 'env'
}


