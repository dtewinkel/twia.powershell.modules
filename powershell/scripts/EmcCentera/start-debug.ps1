﻿<#
This script will run on debug.
It will load in a PowerShell command shell and import the module developed in the project. To end debug, exit this shell.
#>

# Write a reminder on how to end debugging.
$message = "| Exit this shell to end the debug session! |"
$line = "-" * $message.Length
$color = "Cyan"
Write-Host -ForegroundColor $color $line
Write-Host -ForegroundColor $color $message
Write-Host -ForegroundColor $color $line
Write-Host 

# Load the module.
$env:PSModulePath = (Resolve-Path .).Path + ";" + $env:PSModulePath
Import-Module 'EmcCentera' -Verbose

# Happy debugging :-)

$pool = New-CenteraPool 168.159.214.19, 168.159.214.20 -PeaFile .\c1armtesting.pea
$profileClipId = $pool.ProfileClip
$profileClip = ($profileClipId | Open-Clip -Pool $pool)
$dbClipId = $profileClip.Attributes["DatabaseBackup"]
$dbClip = ($dbClipId | Open-Clip -Pool $pool)
$start = (Get-Date) - [TimeSpan]::FromDays(200)
$end = (Get-Date) - [TimeSpan]::FromDays(199.5)
Search-Clips -StartTime $start -EndTime $end -QueryType Existing -Pool $pool -Fields "name" | Select -First 10 | Open-Clip -Pool $pool
Search-Clips -StartTime $start -EndTime $end -Pool $pool | Select -First 10

<#
$methods = @()

& {
    $clip | Get-Member -MemberType Property | % {
        $type = (($_.Definition) -split " ")[0]
        $propName = $_.Name
        $backingName = $propName
        $methods += $propName
        "    public $type $propName { get; internal set; }"
        ""
    }
    ""
    $methods | % {
        "        $_ = clipRef.$_,"
    } 
} | Out-File Clip.cs

#>


#$$pool.Dispose()
