﻿using System;
using EMC.Centera.SDK;

namespace EmcCentera
{
    public class CenteraRetentionClass
    {
        internal CenteraRetentionClass(FPRetentionClass fPRetentionClass)
        {
            Name = fPRetentionClass.Name;
            Period = fPRetentionClass.Period;
        }

        public string Name { get; internal set; }

        public TimeSpan Period { get; internal set; }
    }
}
