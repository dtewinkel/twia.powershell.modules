﻿using System;
using System.Management.Automation;
using EMC.Centera.SDK;

namespace EmcCentera
{
    [Cmdlet(VerbsCommon.Search, "Clips")]
    public class SearchClips : PSCmdlet
    {
        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the pool to read the clip content from:"
            )]
        public CenteraPool Pool { get; set; }

        [Parameter]
        public DateTime StartTime { get; set; }

        [Parameter]
        public DateTime EndTime { get; set; }

        [Parameter]
        public string[] Fields { get; set; }

        [Parameter]
        public QueryType[] QueryType { get; set; }

        protected override void ProcessRecord()
        {
            using (FPQuery myQuery = new FPQuery(Pool.MyPool))
            {
                if (StartTime != DateTime.MinValue)
                {
                    myQuery.StartTime = StartTime;
                }
                else
                {
                    myQuery.UnboundedStartTime = true;
                }
                if (EndTime != DateTime.MinValue)
                {
                    myQuery.EndTime = EndTime;
                }
                else
                {
                    myQuery.UnboundedEndTime = true;
                }

                if (Fields != null)
                {
                    foreach (string field in Fields)
                    {
                        myQuery.SelectField(field);
                    }
                }

                if (QueryType != null && QueryType.Length > 0)
                {
                    foreach (QueryType type in QueryType)
                    {
                        switch (type)
                        {
                            case EmcCentera.QueryType.Existing:
                                myQuery.Type |= FPMisc.QUERY_TYPE_EXISTING;
                                break;

                            case EmcCentera.QueryType.Deleted:
                                myQuery.Type |= FPMisc.QUERY_TYPE_DELETED;
                                break;
                        }
                    }
                }
                else
                {
                    myQuery.Type = FPMisc.QUERY_TYPE_EXISTING | FPMisc.QUERY_TYPE_DELETED;
                }

                myQuery.Execute();

                int queryStatus;

                do
                {
                    FPQueryResult queryResult = new FPQueryResult();
                    queryStatus = myQuery.FetchResult(ref queryResult, -1);

                    switch (queryStatus)
                    {
                        case FPMisc.QUERY_RESULT_CODE_OK:
                            // And store in a collection for later use
                            ProcessResult(queryResult);
                            break;

                        case FPMisc.QUERY_RESULT_CODE_INCOMPLETE: // One or more nodes on centera could not be queried.
                            WriteWarning("Received FP_QUERY_RESULT_CODE_INCOMPLETE error. Is a node down? Trying again...");
                            break;

                        case FPMisc.QUERY_RESULT_CODE_COMPLETE: // Indicate error should have been received after incomplete error.
                            break;

                        case FPMisc.QUERY_RESULT_CODE_ERROR: //Server error
                            WriteWarning("received FP_QUERY_RESULT_CODE_ERROR error, retrying again");
                            break;

                        case FPMisc.QUERY_RESULT_CODE_PROGRESS: // Waiting on results coming back
                            break;

                        case FPMisc.QUERY_RESULT_CODE_END:
                            break;

                        case FPMisc.QUERY_RESULT_CODE_ABORT: // server side issue or start time is later than server time.
                            WriteWarning("Query ended abnormally - FP_QUERY_RESULT_CODE_ABORT error.");
                            break;

                        default: // Unknown error, stop running query
                            WriteWarning("Aborting - received unknown error: " + queryStatus);
                            break;
                    }
                } while (queryStatus != FPMisc.QUERY_RESULT_CODE_END && queryStatus != FPMisc.QUERY_RESULT_CODE_ABORT);
            }
        }


        private void ProcessResult(FPQueryResult queryResult)
        {
            PSObject result = new PSObject();
            result.Properties.Add(new PSNoteProperty("ClipId", queryResult.ClipID));
            result.Properties.Add(new PSNoteProperty("Exists", queryResult.Exists));
            result.Properties.Add(new PSNoteProperty("ResultCode", queryResult.ResultCode));
            result.Properties.Add(new PSNoteProperty("TimeStamp", queryResult.Timestamp));

            if (Fields != null)
            {
                foreach (string field in Fields)
                {
                    string value = queryResult.GetField(field);
                    if (value != null)
                    {
                        result.Properties.Add(new PSNoteProperty(field, value));
                    }
                }
            }

            WriteObject(result);
        }
    }
}