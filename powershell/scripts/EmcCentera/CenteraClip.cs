﻿using System;
using System.Collections.Generic;
using EMC.Centera.SDK;

namespace EmcCentera
{
    public class CenteraClip
    {
        internal CenteraClip(FPClip clipRef)
        {
            Attributes = new Dictionary<string, string>();
            ClipId = clipRef.ClipID;
            ClusterId = clipRef.FPPool.ClusterID;
            CreationDate = clipRef.CreationDate;
            EbrEnabled = clipRef.EBREnabled;
            Modified = clipRef.Modified;
            Name = clipRef.Name;
            NumBlobs = clipRef.NumBlobs;
            OnHold = clipRef.OnHold;
            RetentionClassName = clipRef.RetentionClassName;
            RetentionExpiry = clipRef.RetentionExpiry;
            RetentionPeriod = clipRef.RetentionPeriod;
            Tags = new List<CenteraTag>();
            TotalSize = clipRef.TotalSize;
            if (!string.IsNullOrWhiteSpace(RetentionClassName))
            {
                if (clipRef.FPRetentionClass != null)
                {
                    RetentionClass = new CenteraRetentionClass(clipRef.FPRetentionClass);
                }
            }
            if (EbrEnabled)
            {
                EbrClassName = clipRef.EBRClassName;
                EbrEventTime = clipRef.EBREventTime;
                EbrExpiry = clipRef.EBRExpiry;
                EbrPeriod = clipRef.EBRPeriod;
            }
            foreach (object attributeObject in clipRef.Attributes.ToArray())
            {
                FPAttribute attribute = attributeObject as FPAttribute;
                if (attribute != null)
                {
                    Attributes.Add(attribute.Name, attribute.Value);
                }
            }
            if (clipRef.NumTags > 0)
            {
                FPTagCollection tags = clipRef.Tags;
                foreach (object tagObject in tags.ToArray())
                {
                    using (FPTag tag = tagObject as FPTag)
                    {
                        if (tag != null)
                        {
                            Tags.Add(new CenteraTag(tag));
                        }
                    }
                }
            }
        }

        public Dictionary<string, string> Attributes { get; internal set; }

        public string ClipId { get; internal set; }

        public string ClusterId { get; internal set; }

        public DateTime CreationDate { get; internal set; }

        public string EbrClassName { get; internal set; }

        public bool EbrEnabled { get; internal set; }

        public DateTime EbrEventTime { get; internal set; }

        public DateTime EbrExpiry { get; internal set; }

        public TimeSpan EbrPeriod { get; internal set; }

        public CenteraRetentionClass RetentionClass { get; internal set; }

        public bool Modified { get; internal set; }

        public string Name { get; internal set; }

        public int NumBlobs { get; internal set; }

        public bool OnHold { get; internal set; }

        public string RetentionClassName { get; internal set; }

        public DateTime RetentionExpiry { get; internal set; }

        public TimeSpan RetentionPeriod { get; internal set; }

        public List<CenteraTag> Tags { get; internal set; }

        public long TotalSize { get; internal set; }
    }
}