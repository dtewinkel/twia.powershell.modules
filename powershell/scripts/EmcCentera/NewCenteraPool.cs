﻿using System.IO;
using System.Management.Automation;
using System.Text;
using EMC.Centera.SDK;

namespace EmcCentera
{
    [Cmdlet(VerbsCommon.New, "CenteraPool")]
    public class NewCenteraPool : PSCmdlet
    {
        [Parameter(
            HelpMessage = "Enter the path to the pea file:"
            )]
        public string PeaFile { get; set; }

        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the primary IP address or host name:",
            Position = 0,
            ValueFromRemainingArguments = true
            )]
        public string[] PrimaryHost { get; set; }


        protected override void ProcessRecord()
        {
            StringBuilder connectionString = new StringBuilder();

            connectionString.Append(string.Join(",", PrimaryHost));

            if (!string.IsNullOrWhiteSpace(PeaFile))
            {
                if (!File.Exists(PeaFile))
                {
                    throw new PSInvalidOperationException(string.Format("File not found: '{0}'.", PeaFile));
                }
                connectionString.AppendFormat("?{0}", PeaFile);
            }

            WriteDebug(string.Format("Used connection string: '{0}'.", connectionString));

            FPPool pool = new FPPool(connectionString.ToString());

            WriteObject(new CenteraPool(pool));
        }
    }
}