Push-Location $PSScriptRoot

$PackageRoot = $PSScriptRoot

$LoadingModule = $true

$path = $env:Path 
if (-not $path.Contains($PackageRoot) ) {
	$env:Path += ';' + $PackageRoot
}

dir *.ps1 | % Name | Resolve-Path | Import-Module 

$LoadingModule = $false

Pop-Location
