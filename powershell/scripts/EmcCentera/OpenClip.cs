﻿using System.Management.Automation;
using EMC.Centera.SDK;

namespace EmcCentera
{
    [Cmdlet(VerbsCommon.Open, "Clip")]
    public class OpenClip : PSCmdlet
    {
        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the pool to read the clip from:"
            )]
        public CenteraPool Pool { get; set; }

        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the primary IP address or host name:",
            Position = 0,
            ValueFromPipeline = true,
            ValueFromPipelineByPropertyName = true,
            ValueFromRemainingArguments = true
            )]
        public string[] ClipId { get; set; }


        protected override void ProcessRecord()
        {
            foreach (string clip in ClipId)
            {
                using (FPClip clipRef = Pool.MyPool.ClipOpen(clip, FPMisc.OPEN_FLAT))
                {
                    if (clipRef != null)
                    {
                        WriteObject(new CenteraClip(clipRef));
                    }
                }
            }
        }
    }
}