﻿using System;
using System.Collections.Generic;
using System.Linq;
using EMC.Centera.SDK;

namespace EmcCentera
{
    public class CenteraPool : IDisposable
    {
        internal CenteraPool(FPPool pool)
        {
            MyPool = pool;
            BlobNamingSchemes = pool.BlobNamingSchemes.Split(',').ToList();
            Capacity = pool.Capacity;
            CenteraEdition = pool.CenteraEdition;
            CentraStarVersion = pool.CentraStarVersion;
            ClipBufferSize = pool.ClipBufferSize;
            ClusterId = pool.ClusterID;
            ClusterName = pool.ClusterName;
            ClusterTime = pool.ClusterTime;
            CollisionAvoidanceEnabled = pool.CollisionAvoidanceEnabled;
            EbrSupported = pool.EBRSupported;
            DefaultRetenionScheme = pool.DefaultRetenionScheme;
            DeleteAllowed = pool.DeleteAllowed;
            DeletePools = pool.DeletePools;
            DeletionsLogged = pool.DeletionsLogged;
            ExistsAllowed = pool.ExistsAllowed;
            ExistsPools = pool.ExistsPools;
            FixedRetentionMax = pool.FixedRetentionMax;
            FixedRetentionMin = pool.FixedRetentionMin;
            FreeSpace = pool.FreeSpace;
            HoldAllowed = pool.HoldAllowed;
            HoldPools = pool.HoldPools;
            HoldSupported = pool.HoldSupported;
            MultiClusterFailoverEnabled = pool.MultiClusterFailoverEnabled;
            PoolMappings = pool.PoolMappings;
            PoolProfiles = pool.PoolProfiles;
            PrefetchBufferSize = pool.PrefetchBufferSize;
            PrivilegedDeleteAllowed = pool.PrivilegedDeleteAllowed;
            PrivilegedDeletePools = pool.PrivilegedDeletePools;
            ProfileClip = pool.ProfileClip;
            QueryAllowed = pool.QueryAllowed;
            QueryPools = pool.QueryPools;
            ReadAllowed = pool.ReadAllowed;
            ReadPools = pool.ReadPools;
            ReplicaAddress = pool.ReplicaAddress;
            RetentionClasses = new List<CenteraRetentionClass>();
            RetentionDefault = pool.RetentionDefault;
            RetentionMinMax = pool.RetentionMinMax;
            Timeout = pool.Timeout;
            VariableRetentionMax = pool.VariableRetentionMax;
            VariableRetentionMin = pool.VariableRetentionMin;
            WriteAllowed = pool.WriteAllowed;
            WritePools = pool.WritePools;
            foreach (object retentionClassObject in pool.RetentionClasses.ToArray())
            {
                FPRetentionClass retentionClass = retentionClassObject as FPRetentionClass;
                if (retentionClass != null)
                {
                    RetentionClasses.Add(new CenteraRetentionClass(retentionClass));
                }
            }
        }


        ~CenteraPool()
        {
            Dispose(false);
        }

        protected bool IsDisposed { get; private set; }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !IsDisposed)
            {
                if (MyPool != null)
                {
                    MyPool.Dispose();
                    MyPool = null;
                }
                IsDisposed = true;
            }
        }

        public void Dispose()
        {
            if (!IsDisposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

        public void Close()
        {
            Dispose();
        }

        internal FPPool MyPool { get; set; }

        public List<string> BlobNamingSchemes { get; internal set; }

        public long Capacity { get; internal set; }

        public string CenteraEdition { get; internal set; }

        public string CentraStarVersion { get; internal set; }

        public int ClipBufferSize { get; internal set; }

        public string ClusterId { get; internal set; }

        public string ClusterName { get; internal set; }

        public DateTime ClusterTime { get; internal set; }

        public bool CollisionAvoidanceEnabled { get; internal set; }

        public string DefaultRetenionScheme { get; internal set; }

        public bool DeleteAllowed { get; internal set; }

        public string DeletePools { get; internal set; }

        public bool DeletionsLogged { get; internal set; }

        public bool EbrSupported { get; set; }

        public bool ExistsAllowed { get; internal set; }

        public string ExistsPools { get; internal set; }

        public TimeSpan FixedRetentionMax { get; internal set; }

        public TimeSpan FixedRetentionMin { get; internal set; }

        public long FreeSpace { get; internal set; }

        public bool HoldAllowed { get; internal set; }

        public string HoldPools { get; internal set; }

        public bool HoldSupported { get; internal set; }

        public bool MultiClusterFailoverEnabled { get; internal set; }

        public string PoolMappings { get; internal set; }

        public string PoolProfiles { get; internal set; }

        public int PrefetchBufferSize { get; internal set; }

        public bool PrivilegedDeleteAllowed { get; internal set; }

        public string PrivilegedDeletePools { get; internal set; }

        public string ProfileClip { get; internal set; }

        public bool QueryAllowed { get; internal set; }

        public string QueryPools { get; internal set; }

        public bool ReadAllowed { get; internal set; }

        public string ReadPools { get; internal set; }

        public string ReplicaAddress { get; internal set; }

        public List<CenteraRetentionClass> RetentionClasses { get; internal set; }

        public TimeSpan RetentionDefault { get; internal set; }

        public bool RetentionMinMax { get; internal set; }

        public int Timeout { get; internal set; }

        public TimeSpan VariableRetentionMax { get; internal set; }

        public TimeSpan VariableRetentionMin { get; internal set; }

        public bool WriteAllowed { get; internal set; }

        public string WritePools { get; internal set; }
    }
}