﻿using System.Collections.Generic;
using EMC.Centera.SDK;

namespace EmcCentera
{
    public class CenteraTag
    {
        internal CenteraTag(FPTag fromTag)
        {
            Name = fromTag.Name;
            BlobSize = fromTag.BlobSize;
            BlobStatus = (BlobStatus)fromTag.BlobStatus;
            Attributes = new Dictionary<string, string>();
            foreach (object attributeObject in fromTag.Attributes.ToArray())
            {
                FPAttribute attribute = attributeObject as FPAttribute;
                if (attribute != null)
                {
                    Attributes.Add(attribute.Name, attribute.Value);
                }
            }
        }

        public string Name { get; internal set; }
        public long BlobSize { get; internal set; }
        public BlobStatus BlobStatus { get; internal set; }
        public Dictionary<string, string> Attributes { get; internal set; }
    }
}