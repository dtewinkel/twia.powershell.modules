﻿using System.Linq;
using System.Management.Automation;
using EMC.Centera.SDK;

namespace EmcCentera
{
    [Cmdlet(VerbsCommon.Get, "ClipContent")]
    public class GetClipContent : PSCmdlet
    {
        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the pool to read the clip content from:",
            Position = 0
            )]
        public CenteraPool Pool { get; set; }

        [Parameter(
            Mandatory = true,
            HelpMessage = "ID of the clip to get content from:",
            Position = 1
            )]
        public string ClipId { get; set; }

        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the name of the tag to retrieve the content from:",
            Position = 2
            )]
        public string TagName { get; set; }

        [Parameter(
            Mandatory = true,
            HelpMessage = "Enter the name of the file to store the content in:",
            Position = 3
            )]
        public string FileName { get; set; }


        protected override void ProcessRecord()
        {
            using (FPClip clipRef = new FPClip(Pool.MyPool, ClipId, FPMisc.OPEN_FLAT))
            {
                using (FPTag tag = clipRef.Tags.ToArray().Cast<FPTag>().FirstOrDefault(t => t.Name == TagName))
                {
                    if (tag != null)
                    {
                        FPStream streamRef = new FPStream(FileName, "wb");
                        tag.BlobRead(streamRef, FPMisc.OPTION_DEFAULT_OPTIONS);
                        streamRef.Close();
                        WriteObject(FileName);
                    }
                    else
                    {
                        WriteWarning(string.Format("Tag '{0}' not found on clip '{1}'.", TagName, ClipId));
                    }
                }
            }
        }
    }
}