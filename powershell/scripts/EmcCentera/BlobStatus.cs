﻿namespace EmcCentera
{
    public enum BlobStatus
    {
        NoBlob = -1,
        NotAvailable = 0,
        Exists = 1,
    }
}