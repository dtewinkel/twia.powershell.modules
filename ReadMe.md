﻿# TWIA PowerShell Modules

This project contains a number of PowerShell modules. 

These modules serve two purposes: Be useful (they are at least to me) and learn about PowerShell modules.

All modules require at least PowerShell version 3.

# The modules
 
## Environment

This module contains functions to deal with environment variables:
```powershell
Get-EnvironmentVariable
Test-EnvironmentVariable
Remove-EnvironmentVariable
Set-EnvironmentVariable
```
These modules provide more control then the standard `env:` drive in PowerShell allows.

# Development

## Source Control

The (original) sources for this project are stored in a [GIT][] repository on [Bitbucket][]: [Twia.PowerShell.Modules](https://bitbucket.org/dtewinkel/twia.powershell.modules).
 
## Software
This project is built using:
 
* Visual Studio 2013.
* PowerShell version 3.

Some tools that I found very useful:

* [ReSharper][] by [JetBrains][].
* [PowerShell Tools for Visual Studio](http://adamdriscoll.github.io/poshtools/), if not just for the syntax highlighting and support in Visual Studio.
* [MarkdownPad 2](http://markdownpad.com/) to edit this file.
* [Visual Studio Spell Checker](http://vsspellchecker.codeplex.com/), because my spelling is far from perfect.
* [Pandoc](http://johnmacfarlane.net/pandoc/) to convert the ReadMe.md file to html.

# Copyright and License

**Copyright © 2014, Daniël te Winkel. All rights reserved.**

See License.txt for copyright and license details and, if applicable, copyright notices of included open source components.

[NuGet]: http://www.nuget.org/ "NuGet Package Manager"
[GIT]: http://git-scm.com/ "Git distributed source control system"
[Bitbucket]: https://bitbucket.org/ "Hosting of Git and Mercurial repositories"
[JetBrains]: http://www.jetbrains.com
[ReSharper]: http://www.jetbrains.com/resharper/
[https://bitbucket.org/dtewinkel/twia.powershell.modules downloads]: https://bitbucket.org/dtewinkel/twia.powershell.modules/downloads "Twia.PowerShell.Modules downloads on Bitbucket"
